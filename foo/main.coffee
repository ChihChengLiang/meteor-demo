@students = new Mongo.Collection "students"


if Meteor.isClient
  list = [
    {name: "abc"}
    {name: "bcd"}
  ]
  Template.body.helpers
    list: ()-> list

  Template.main.events
    'click #btn': () ->
      console.log "fooo"
      Meteor.call "add_student"

  Template.foooooo.helpers
    students: () -> students.find()


if Meteor.isServer
  fixture_students = [
    {name:"alice"}
    {name:"bob"}
  ]
  Meteor.startup ->
    console.log "foo"
    if students.find().count() ==0
      for s in fixture_students
        students.insert s

  Meteor.methods
    "add_student": ->
      students.insert {name:"fooo"}

  Meteor.publish "students", ->
    return students.find()

Router.route "/", ->
  this.subscribe "students"
  @render "main"

Router.route "other_page", ->
  @render "foooooo"